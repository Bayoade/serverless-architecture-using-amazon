﻿using ServerlessArchitecture.Core.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServerlessArchitecture.Core.IService
{
    public interface IPlayerDynamoService
    {
        Task<Player> GetPlayerAsync(int id);

        Task<IList<Player>> GetAllPlayers();

        Task SavePlayerAsync(Player player);

        Task DeleteByIdPlayerAsync(int id);

        Task UpdatePlayerAsync(Player player);
    }
}
