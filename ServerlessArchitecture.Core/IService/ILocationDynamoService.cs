﻿using ServerlessArchitecture.Core.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServerlessArchitecture.Core.IService
{
    public interface ILocationDynamoService
    {
        Task<Location> GetLocationAsync(int id);

        Task<IList<Location>> GetAllLocation();

        Task SaveLocationAsync(Location location);

        Task DeleteByIdLocationAsync(int id);

        Task UpdateLocationAsync(Location location);
    }
}
