﻿using Amazon.DynamoDBv2.DataModel;

namespace ServerlessArchitecture.Core.Model
{
    //[DynamoDBTable("Location")]
    public class Location
    {
        [DynamoDBHashKey]
        public int Id { get; set; }

        [DynamoDBProperty]
        public string Name { get; set; }

        [DynamoDBProperty]
        public string Description { get; set; }
    }
}
