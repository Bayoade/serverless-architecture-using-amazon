﻿using Amazon.DynamoDBv2.DataModel;
using System.Collections.Generic;

namespace ServerlessArchitecture.Core.Model
{
    //[DynamoDBTable("Player")]
    public class Player
    {
        [DynamoDBHashKey]
        public int Id { get; set; }

        [DynamoDBProperty]
        public string Name { get; set; }

        [DynamoDBProperty]
        public int HitPoints { get; set; }

        [DynamoDBProperty]
        public int Gold { get; set; }

        [DynamoDBProperty]
        public int Level { get; set; }

        [DynamoDBProperty]
        public List<string> Items { get; set; }
    }
}
