﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServerlessArchitecture.Core.IRepository
{
    public interface IDynamoDbContext<T> : IDisposable where T : class
    {
        Task<T> GetByIdAsync(int id);

        Task SaveAsync(T item);

        Task DeleteByIdAsync(int id);

        Task<IList<T>> GetAllAsync();
    }
}
