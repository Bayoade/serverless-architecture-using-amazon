﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerlessArchitecture.Data.Sql.DbContext
{
    public class DynamoDbOptions
    {
        public string Player { get; set; }
        public string Location { get; set; }
    }
}
