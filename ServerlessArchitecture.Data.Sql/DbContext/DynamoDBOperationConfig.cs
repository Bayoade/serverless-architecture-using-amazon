﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using ServerlessArchitecture.Core.IRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServerlessArchitecture.Data.Sql.DbContext
{
    public class DynamoDbContext<T> : DynamoDBContext, IDynamoDbContext<T>
    where T : class
    {
        private DynamoDBOperationConfig _config;

        public DynamoDbContext(IAmazonDynamoDB client, string tableName)
            : base(client)
        {
            _config = new DynamoDBOperationConfig()
            {
                OverrideTableName = tableName,
                ConsistentRead = true
            };
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await base.LoadAsync<T>(id, _config);
        }

        public async Task SaveAsync(T item)
        {
            await base.SaveAsync(item, _config);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await GetByIdAsync(id);
            await base.DeleteAsync(item, _config);
        }

        public async Task<IList<T>> GetAllAsync()
        {
            return await base.ScanAsync<T>(new List<ScanCondition>()).GetRemainingAsync();
        }
    }
}
