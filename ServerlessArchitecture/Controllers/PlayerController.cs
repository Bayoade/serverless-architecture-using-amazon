﻿using Microsoft.AspNetCore.Mvc;
using ServerlessArchitecture.Core.IService;
using ServerlessArchitecture.Core.Model;
using System.Threading.Tasks;

namespace ServerlessArchitecture.Controllers
{
    public class PlayerController : Controller
    {
        private readonly IPlayerDynamoService _playerDynamoService;

        public PlayerController(IPlayerDynamoService playerDynamoService)
        {
            _playerDynamoService = playerDynamoService;
        }
        public async Task<IActionResult> Index()
        {
            var players = await _playerDynamoService.GetAllPlayers();

            return View(players);
        }

        public IActionResult AddPlayer()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddPlayer(Player player)
        {
            await _playerDynamoService.SavePlayerAsync(player);

            return RedirectToAction("Index");
        }

        public IActionResult UpdatePlayer()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> UpdatePlayer(Player player)
        {
            await _playerDynamoService.UpdatePlayerAsync(player);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> GetPlayer(int id)
        {
            var player = await _playerDynamoService.GetPlayerAsync(id);

            return RedirectToAction("Index");
        }
    }
}