﻿using Microsoft.AspNetCore.Mvc;
using ServerlessArchitecture.Core.IService;
using ServerlessArchitecture.Core.Model;
using System.Threading.Tasks;

namespace ServerlessArchitecture.Controllers
{
    public class LocationController : Controller
    {
        private readonly ILocationDynamoService _locationService;

        public LocationController(ILocationDynamoService locationService)
        {
            _locationService = locationService;
        }

        public async Task<IActionResult> Index()
        {
            var location = await _locationService.GetAllLocation();
            return View(location);
        }

        public IActionResult AddLocation()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddLocation(Location location)
        {
            await _locationService.SaveLocationAsync(location);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteLocation(int id)
        {
            await _locationService.DeleteByIdLocationAsync(id);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> GetLocation(int id)
        {
            var location = await _locationService.GetLocationAsync(id);

            return View(location);
        }

        public IActionResult UpdateLocation()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateLocation(Location location)
        {
            await _locationService.UpdateLocationAsync(location);

            return RedirectToAction("Index");
        }
    }
}