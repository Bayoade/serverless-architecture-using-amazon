﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System.IO;

namespace ServerlessArchitecture.Bootstrap
{
    public static class ConfigureLogger
    {
        public static void AddLoggerConfiguration(this IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory logger)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.File(Path.Combine(env.ContentRootPath, "serverlessarchitecture-log.txt"))
                .CreateLogger();

            logger.AddSerilog();
        }
    }
}
