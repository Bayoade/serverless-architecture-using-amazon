﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ServerlessArchitecture.AppService;

namespace ServerlessArchitecture.Bootstrap
{
    public static class ConfigureAutoMapperProfiles
    {
        public static void AddAutoMapperProfile(this IServiceCollection services)
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<ServiceMapperProfile>();
            });
        }
    }
}
