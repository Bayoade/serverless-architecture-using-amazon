﻿using Microsoft.Extensions.Configuration;

namespace ServerlessArchitecture.Bootstrap
{
    public static class Ioc
    {
        public static IConfiguration Configuration { get; set; }
    }
}
