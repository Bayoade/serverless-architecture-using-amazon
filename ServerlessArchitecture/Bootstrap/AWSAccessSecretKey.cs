﻿namespace ServerlessArchitecture.Bootstrap
{
    public class AWSAccessSecretKey
    {
        public string AccessKeyId { get; set; }

        public string AccessSecretKeyId { get; set; }
    }
}
