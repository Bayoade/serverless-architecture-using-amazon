﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.Runtime;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServerlessArchitecture.Core.IRepository;
using ServerlessArchitecture.Core.Model;
using ServerlessArchitecture.Data.Sql.DbContext;

namespace ServerlessArchitecture.Bootstrap
{
    public static class ConfigureAWSOptions 
    {
        public static void AddAWSSOptionConfiguration(this IServiceCollection services)
        {
            var awsAcessSecretKey = new AWSAccessSecretKey();
            var awsOptions = Ioc.Configuration.GetAWSOptions();
            Ioc.Configuration.GetSection("AWS").Bind(awsAcessSecretKey);

            awsOptions.Region = RegionEndpoint.EUWest1;
            awsOptions.Credentials = new BasicAWSCredentials(awsAcessSecretKey.AccessKeyId, awsAcessSecretKey.AccessSecretKeyId);
            awsOptions.ProfilesLocation = "/dynamo/aws";

            services.AddDefaultAWSOptions(awsOptions);

            var client = awsOptions.CreateServiceClient<IAmazonDynamoDB>();
            var dynamoDbOptions = new DynamoDbOptions();
            ConfigurationBinder.Bind(Ioc.Configuration.GetSection("DynamoDbTables"), dynamoDbOptions);

            services.AddScoped<IDynamoDbContext<Player>>(provider => new DynamoDbContext<Player>(client, dynamoDbOptions.Player));
            services.AddScoped<IDynamoDbContext<Location>>(provider => new DynamoDbContext<Location>(client, dynamoDbOptions.Location));
        }
    }
}
