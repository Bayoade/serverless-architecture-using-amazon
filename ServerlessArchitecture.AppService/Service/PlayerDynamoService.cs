﻿using AutoMapper;
using ServerlessArchitecture.Core.IRepository;
using ServerlessArchitecture.Core.IService;
using ServerlessArchitecture.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServerlessArchitecture.AppService.Service
{
    public class PlayerDynamoService : IPlayerDynamoService
    {
        private IDynamoDbContext<Player> _playerContext;

        public PlayerDynamoService(
            IDynamoDbContext<Player> playerContext)
        {
            _playerContext = playerContext;
        }

        public async Task<Player> GetPlayerAsync(int id)
        {
            return await _playerContext.GetByIdAsync(id);
        }

        public Task<IList<Player>> GetAllPlayers()
        {
            return _playerContext.GetAllAsync();
        }

        public Task SavePlayerAsync(Player player)
        {
            return _playerContext.SaveAsync(player);
        }

        public Task DeleteByIdPlayerAsync(int id)
        {
            return _playerContext.DeleteByIdAsync(id);
        }

        public async Task UpdatePlayerAsync(Player player)
        {
            var dbPlayer = await _playerContext.GetByIdAsync(player.Id);

            Mapper.Map(player, dbPlayer);

            await _playerContext.SaveAsync(dbPlayer);
        }
    }
}
