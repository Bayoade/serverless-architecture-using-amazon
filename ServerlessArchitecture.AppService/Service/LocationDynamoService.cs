﻿using AutoMapper;
using ServerlessArchitecture.Core.IRepository;
using ServerlessArchitecture.Core.IService;
using ServerlessArchitecture.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ServerlessArchitecture.AppService.Service
{
    public class LocationDynamoService : ILocationDynamoService
    {
        private readonly IDynamoDbContext<Location> _locationDbContext;

        public LocationDynamoService(IDynamoDbContext<Location> locationDbContext)
        {
            _locationDbContext = locationDbContext;
        }

        public Task DeleteByIdLocationAsync(int id)
        {
            return _locationDbContext.DeleteByIdAsync(id);
        }

        public async Task<IList<Location>> GetAllLocation()
        {
            return await _locationDbContext.GetAllAsync();
        }

        public Task<Location> GetLocationAsync(int id)
        {
            return _locationDbContext.GetByIdAsync(id);
        }

        public async Task SaveLocationAsync(Location location)
        {
            await _locationDbContext.SaveAsync(location);
        }

        public async Task UpdateLocationAsync(Location location)
        {
            var dbLocation = await _locationDbContext.GetByIdAsync(location.Id);

            Mapper.Map(location, dbLocation);

            await _locationDbContext.SaveAsync(dbLocation);
        }
    }
}
